package ru.tsc.vinokurov.tm;

import ru.tsc.vinokurov.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
