package ru.tsc.vinokurov.tm.controller;

import ru.tsc.vinokurov.tm.api.ICommandController;
import ru.tsc.vinokurov.tm.api.ICommandService;
import ru.tsc.vinokurov.tm.model.Command;
import ru.tsc.vinokurov.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showHelp() {
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showAbout() {
        System.out.println("Developer: Aleksey Vinokurov");
        System.out.println("Email: avv96@yandex.ru");
    }

    @Override
    public void showVersion() {
        String taskManagerVersion = "1.9.0";
        System.out.printf("Version: %s\n", taskManagerVersion);
    }

    @Override
    public void showErrorArgument(String arg) {
        System.err.printf("Error! %s is not valid argument!\n", arg);
    }

    @Override
    public void showErrorCommand(String arg) {
        System.err.printf("Error! %s is not valid command!\n", arg);
    }

    @Override
    public void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int processors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        final long totalMemory = runtime.totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("[INFO]");
        System.out.println("Available processors: " + processors);
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
