package ru.tsc.vinokurov.tm.api;

public interface ICommandController {

    void showHelp();

    void showCommands();

    void showArguments();

    void showAbout();

    void showVersion();

    void showErrorArgument(String arg);

    void showErrorCommand(String arg);

    void showWelcome();

    void showSystemInfo();

}
