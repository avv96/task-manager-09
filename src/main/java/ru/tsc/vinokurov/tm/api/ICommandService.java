package ru.tsc.vinokurov.tm.api;

import ru.tsc.vinokurov.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
